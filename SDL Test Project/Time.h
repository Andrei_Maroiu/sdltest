#pragma once

#include <chrono>

class Time
{
private:

	class TimeAux
	{
	private:
		float timeScale;
		double realDelta;
		double deltaTime;
		double unscaledDelta;
		double realTime;
		double gameTime;
		std::chrono::time_point<std::chrono::system_clock> last;
		std::chrono::time_point<std::chrono::system_clock> now;
		TimeAux();
	public:
		TimeAux(const TimeAux&) = delete;
		TimeAux& operator=(const TimeAux&) = delete;

		friend class Time;
	};

	Time();
	static TimeAux aux;

	static void UpdateTime();
	static void updateDone();
	static void initTime();

	friend class Game;

	Time(const Time&) = delete;
	Time& operator= (const Time&) = delete;
public:
	static double getRealDelta();
	static double getDeltaTime();
	static double getUnscaledDeltaTime();
	static double getRealTime();
	static double getGameTime();
	static float getTimeScale();
	static void setTimeScale(float timeScale);
};