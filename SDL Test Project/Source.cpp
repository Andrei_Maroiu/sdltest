#include <iostream>
#include "Game.h"

int main(int argc, char **argv)
{
	Game* game = new Game();

	game->initGame("Ceva", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 1000, 500, false);

    if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
    {
        std::cout << "error initializing img library";
    }
    
	game->start();

    delete game;

	return 0;
}