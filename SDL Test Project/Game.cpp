#include "Game.h"

SDL_Texture* texture;

Game::Game() : running { false }, window {nullptr}, renderer {nullptr}, count {0}
{
    xOffset = 1;
    yOffset = 1;
}

Game::Game(const char* title, int x, int y, int width, int height, bool fullscreen) : running { false }
{
    initGame(title, x, y, width, height, fullscreen);
}

Game::~Game()
{
    clean();
}

void Game::initGame(const char* title, int x, int y, int width, int height, bool fullscreen)
{
    int flags = 0;
    if (fullscreen)
    {
        flags = SDL_WINDOW_FULLSCREEN;
    }
    if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
    {
        std::cout << "Subsystem initialised!\n";
        window = SDL_CreateWindow(title, x, y, width, height, flags);

        if (window)
        {
            std::cout << "Window created\n";
        }

        renderer = SDL_CreateRenderer(window, -1, 0);

        if (renderer)
        {
            SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
            std::cout << "Renderer created!\n";
        }

        scale.setScreenBounds(width, height);

        SDL_Surface* temp = IMG_Load("dvd.png");

        img = SDL_CreateTextureFromSurface(renderer, temp);

        SDL_FreeSurface(temp);

        running = true;
    }

}

void Game::update()
{
    /*std::cout << count << "\n";
    ++count;*/

    float xOffset = (3.0 * Time::getDeltaTime()) * this->xOffset;
    float yOffset = (3.0 * Time::getDeltaTime()) * this->yOffset;

    x += xOffset;
    y += yOffset;

    if (x > 9 || x < -9)
    {
        this->xOffset *= -1;
    }
    if (y > 4 || y < -4)
    {
        this->yOffset *= -1;
    }
}

void Game::render()
{
    SDL_RenderClear(renderer);

    SDL_Rect rect;
    int w = 2, h = 2;

    scale.pointToPixel(rect, x, y, w, h);

    SDL_RenderCopy(renderer, img, NULL, &rect);

    /*SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderFillRect(renderer, &rect);
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);*/

    SDL_RenderPresent(renderer);
}

void Game::clean()
{
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    SDL_Quit();
    std::cout << "Game cleaned!\n";
}

void Game::handleEvents()
{
    SDL_Event event;
    SDL_PollEvent(&event);

    switch (event.type)
    {
    case SDL_QUIT:
        running = false;
        break;

    default:
        break;
    }
}

void Game::start()
{
    Time::initTime();

    double delta = 0;
    double maxFPS = 120.0;
    double ns;

    int fpsCounter = 0;
    double fpsTimer = 0.0;

    if (maxFPS)
    {
        ns = 1.0 / maxFPS;
    }
    else
    {
        ns = 0;
    }

    while (running)
    {
        Time::UpdateTime();

        delta += Time::getRealDelta();
        fpsTimer += Time::getRealDelta();

        if (delta >= ns)
        {
            handleEvents();
            update();
            render();
            ++fpsCounter;

            Time::updateDone();
            delta = 0;
        }

        if (fpsTimer >= 1.0)
        {
            std::cout << "FPS: " << fpsCounter << "\n";
            fpsCounter = 0;
            fpsTimer = 0.0;
        }
    }

    clean();
}

bool Game::isRunning()
{
    return running;
}
