#pragma once
#include "ScreenScale.h"
#include <iostream>
#include "Time.h"
#include <SDL_image.h>

class Game
{
private:
	bool running;
	SDL_Window* window;
	SDL_Renderer* renderer;

	SDL_Texture* img = NULL;

	ScreenScale scale;
	float xOffset = 1;
	float yOffset = 1;
	float x = 0, y = 0;
	int count;

public:
	Game();
	Game(const char* title, int x, int y, int width, int height, bool fullscreen);
	~Game();

	void initGame(const char* title, int x, int y, int width, int height, bool fullscreen);
	void update();
	void render();
	void clean();
	void handleEvents();

	void start();

	bool isRunning();

};

