#pragma once
#include <SDL.h>

class ScreenScale
{
private:
	int heightUnit;
	int widthUnit;

	int width;
	int height;

	const static int wUnitCount;
	const static int hUnitCount;

public:
	ScreenScale(int width, int height);
	ScreenScale();

	void setScreenBounds(int width, int height);

	int getHeightUnit();
	int getWidthUnit();

	int getMiddleX();
	int getMiddleY();

	void pointToPixel(SDL_Rect& rect, float x, float y, float w, float h);
};

