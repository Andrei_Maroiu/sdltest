#include "ScreenScale.h"

const int ScreenScale::hUnitCount = 10;
const int ScreenScale::wUnitCount = 20;

ScreenScale::ScreenScale(int width, int height)
	: width {width}, height {height} 
{
	heightUnit = height / hUnitCount;
	widthUnit = width / wUnitCount;
}

ScreenScale::ScreenScale() : width{ 1 }, height{ 1 }, heightUnit{ 1 }, widthUnit{ 1 } {}

void ScreenScale::setScreenBounds(int width, int height)
{
	this->width = width;
	this->height = height;
	heightUnit = height / hUnitCount;
	widthUnit = width / wUnitCount;
}

int ScreenScale::getHeightUnit()
{
	return heightUnit;
}

int ScreenScale::getWidthUnit()
{
	return widthUnit;
}

int ScreenScale::getMiddleX()
{
	return width / 2;
}

int ScreenScale::getMiddleY()
{
	return height / 2;
}

void ScreenScale::pointToPixel(SDL_Rect& rect, float x, float y, float w, float h)
{
	rect.w = w * widthUnit;
	rect.h = h * heightUnit;
	rect.x = x * widthUnit + width / 2 - rect.w / 2;
	rect.y = y * -1 * heightUnit + height / 2 - rect.h / 2;
}
