
#include "Time.h"

//std::chrono::time_point<std::chrono::system_clock> Time::last = std::chrono::system_clock::now();
//std::chrono::time_point<std::chrono::system_clock> Time::now = std::chrono::system_clock::now();
//double Time::realDelta = 0.0;
//double Time::gameTime = 0.0;
//double Time::deltaTime = 0.0;
//double Time::realTime = 0.0;
//double Time::unscaledDelta = 0.0;
//float Time::timeScale = 1.0f;

Time::TimeAux::TimeAux()
    : now{ std::chrono::system_clock::now() }, last{ std::chrono::system_clock::now() }, realDelta{ 0.0 },
    gameTime{ 0.0 }, deltaTime{ 0.0 }, realTime{ 0.0 }, unscaledDelta{ 0.0 }, timeScale{ 1.0f } {}

Time::TimeAux Time::aux;

void Time::UpdateTime()
{
    aux.now = std::chrono::system_clock::now();

    std::chrono::duration<double> dif = aux.now - aux.last;
    aux.realDelta = dif.count();
    aux.unscaledDelta += aux.realDelta;
    aux.deltaTime += (aux.realDelta * aux.timeScale);
    aux.realTime += aux.realDelta;
    aux.gameTime += (aux.realDelta * aux.timeScale);

    aux.last = aux.now;
}

double Time::getRealDelta()
{
    return aux.realDelta;
}

double Time::getDeltaTime()
{
    return aux.deltaTime;
}

double Time::getUnscaledDeltaTime()
{
    return aux.unscaledDelta;
}

double Time::getRealTime()
{
    return aux.realTime;
}

double Time::getGameTime()
{
    return aux.gameTime;
}

float Time::getTimeScale()
{
    return aux.timeScale;
}

void Time::setTimeScale(float timeScale)
{
    if (timeScale >= 0.0f)
    {
        Time::aux.timeScale = timeScale;
    }
}

void Time::updateDone()
{
    aux.unscaledDelta = 0.0;
    aux.deltaTime = 0.0;
}

void Time::initTime()
{
    aux.last = std::chrono::system_clock::now();
    aux.now = std::chrono::system_clock::now();
    aux.realDelta = 0.0;
    aux.deltaTime = 0.0;
    aux.unscaledDelta = 0.0;
    aux.gameTime = 0.0;
    aux.realTime = 0.0;
    aux.timeScale = 1.0f;
}